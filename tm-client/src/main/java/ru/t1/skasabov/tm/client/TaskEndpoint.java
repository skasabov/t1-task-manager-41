package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    @Override
    public TaskGetByProjectIdResponse findAllByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        return ITaskEndpoint.newInstance().findAllByProjectId(request);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return ITaskEndpoint.newInstance().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        return ITaskEndpoint.newInstance().unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        return ITaskEndpoint.newInstance().changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        return ITaskEndpoint.newInstance().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        return ITaskEndpoint.newInstance().clearTasks(request);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return ITaskEndpoint.newInstance().createTask(request);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        return ITaskEndpoint.newInstance().getTaskById(request);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        return ITaskEndpoint.newInstance().getTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        return ITaskEndpoint.newInstance().listTasks(request);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return ITaskEndpoint.newInstance().removeTaskById(request);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return ITaskEndpoint.newInstance().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return ITaskEndpoint.newInstance().updateTaskById(request);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return ITaskEndpoint.newInstance().updateTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        return ITaskEndpoint.newInstance().startTaskById(request);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        return ITaskEndpoint.newInstance().startTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        return ITaskEndpoint.newInstance().completeTaskById(request);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return ITaskEndpoint.newInstance().completeTaskByIndex(request);
    }

}
