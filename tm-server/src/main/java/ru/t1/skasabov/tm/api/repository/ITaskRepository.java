package ru.t1.skasabov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    Boolean existsByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByCreatedForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByStatusForUser(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByNameForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    Task findOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    Task findOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_task WHERE id IN (SELECT id FROM tm_task " +
            "WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeAllForUser(@NotNull @Param("userId") String userId);

    @Insert("INSERT INTO tm_task (id, name, description, created, user_id, project_id, status, begin_date, end_date) " +
            "VALUES (#{id}, #{name}, #{description}, #{created}, #{userId}, #{projectId}, #{status}, " +
            "#{dateBegin}, #{dateEnd})")
    void add(@NotNull Task model);

    @NotNull
    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAll();

    @NotNull
    @Select("SELECT * FROM tm_task ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByCreated();

    @NotNull
    @Select("SELECT * FROM tm_task ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByStatus();

    @NotNull
    @Select("SELECT * FROM tm_task ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllSortByName();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_task WHERE id = #{id}")
    Boolean existsById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    Task findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    Task findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_task")
    int getSize();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOne(@NotNull Task model);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_task WHERE id IN (SELECT id FROM tm_task LIMIT 1 OFFSET #{index})")
    void removeOneByIndex(@NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_task")
    void removeAll();

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, status = #{status}, " +
            "project_id = #{projectId} where user_id = #{userId} AND id = #{id}")
    void update(@NotNull Task task);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "begin_date"),
            @Result(property = "dateEnd", column = "end_date")
    })
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

}
