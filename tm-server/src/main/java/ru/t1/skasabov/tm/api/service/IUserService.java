package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.dto.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    User add(@Nullable User model);

    @NotNull
    List<User> findAll();

    @NotNull
    Collection<User> addAll(@Nullable Collection<User> models);

    @NotNull
    Collection<User> set(@Nullable Collection<User> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    User removeOne(@Nullable User model);

    @Nullable
    User removeOneById(@Nullable String id);

    @Nullable
    User removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<User> collection);

    void removeAll();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}
