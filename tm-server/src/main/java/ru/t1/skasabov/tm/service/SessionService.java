package ru.t1.skasabov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.ISessionService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.dto.model.Session;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<Session> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            for (@NotNull final Session session : collection) {
                repository.removeOne(session);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Session add(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(model);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Session> addAll(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            for (@NotNull final Session model : models) {
                repository.add(model);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Session> set(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAll();
            for (@NotNull final Session model : models) {
                repository.add(model);
            }
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull List<Session> sessions;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAll();
        }
        return sessions;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(id);
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable Session session;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIndex(index);
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session removeOne(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOne(model);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(id);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOneById(id);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOneByIndex(index);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAll();
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        int size;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            size = repository.getSize();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsSession;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            existsSession = repository.existsById(id);
        }
        return existsSession;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAllForUser(userId);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsSession;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            existsSession = repository.existsByIdForUser(userId, id);
        }
        return existsSession;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Session> sessions;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAllForUser(userId);
        }
        return sessions;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIdForUser(userId, id);
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable Session session;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIndexForUser(userId, index);
        }
        return session;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        try(@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            size = repository.getSizeForUser(userId);
        }
        return size;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOneByIdForUser(userId, id);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOneByIndexForUser(userId, index);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
        return model;
    }

}
