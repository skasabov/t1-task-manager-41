package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.dto.model.Task;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable Task model);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable Sort sortType);

    @NotNull
    Collection<Task> addAll(@Nullable Collection<Task> models);

    @NotNull
    Collection<Task> set(@Nullable Collection<Task> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    Task findOneById(@Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    Task removeOne(@Nullable Task model);

    @Nullable
    Task removeOneById(@Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<Task> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Sort sortType);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name, @Nullable String description,
                @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
