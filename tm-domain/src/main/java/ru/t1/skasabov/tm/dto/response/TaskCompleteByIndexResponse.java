package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.Task;

@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
